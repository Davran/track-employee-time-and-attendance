FROM node:carbon as frontend-builder
RUN npm i -g npm@5.6.0


WORKDIR /home/client
COPY client /home/client
RUN npm install \
&& npm run build

# If you are building your code for production
# RUN npm install --only=production
FROM node:carbon

RUN apt-get update \
  && apt-get -y install nginx-light supervisor -y \
  && rm -rf /var/lib/apt/lists/*
COPY docker/nginx-default.conf /etc/nginx/sites-enabled/default
COPY docker/supervisord-server.conf /etc/supervisor/conf.d/supervisord-server.conf
COPY docker/supervisord-nginx.conf /etc/supervisor/conf.d/supervisord-nginx.conf

COPY --from=frontend-builder /home/client/dist /home/client

WORKDIR /home/server
COPY server /home/server
RUN npm install

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]

EXPOSE 80
