import {observable} from 'mobx'
import {attr} from '../../annotations/model'

export class DefaultModel {
  @attr()
  @observable
  id: string
  
  @attr()
  @observable
  createdAt: string
  
  @attr()
  @observable
  updatedAt: string
}

export default DefaultModel
