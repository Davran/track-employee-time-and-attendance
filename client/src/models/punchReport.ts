import {observable} from 'mobx'
import {attr} from '../common/annotations/model'
import Schedule from './schedule'
import Employee from './employee'

namespace PunchReport {
  export class Model {
    @attr()
    @observable
    id: string
  
    @attr()
    @observable
    createdAt: string
  
    @attr()
    @observable
    updatedAt: string
    
    @attr()
    @observable
    lat: number
    
    @attr()
    @observable
    lon: number
    
    @attr()
    @observable
    city: string
    
    @attr()
    @observable
    photo: string
    
    @attr()
    @observable
    status: 'allowed' | 'disallowed' | 'pending'
    
    @attr()
    @observable
    action: 'in' | 'out'
  
    @attr()
    @observable
    reasons: Employee.PunchResultReasons
    
    @attr()
    @observable
    employee: Employee.Model
    
    @attr()
    @observable
    schedule: Schedule.ModalWithoutEmployee
  }
  
  export interface Records {
  
  }
}

export default PunchReport
