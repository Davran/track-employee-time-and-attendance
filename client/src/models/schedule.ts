import {observable} from 'mobx'
import {attr} from '../common/annotations/model'
import Employee from './employee'
import DefaultModel from '../common/models/base/base'

namespace Schedule {
  export class Model{
    @attr()
    @observable
    id: string
  
    @attr()
    @observable
    createdAt: number
  
    @attr()
    @observable
    updatedAt: number
    
    @attr()
    @observable
    startAt: string
    
    @attr()
    @observable
    endAt: string
    
    @attr()
    @observable
    dayOfTheWeek: number
  
    @attr({optional: true})
    @observable
    employee: Employee.Model | string
  }
  
  export class ModalWithoutEmployee {
    @attr()
    @observable
    id: string
  
    @attr()
    @observable
    createdAt: number
  
    @attr()
    @observable
    updatedAt: number
  
    @attr()
    @observable
    startAt: string
  
    @attr()
    @observable
    endAt: string
  
    @attr()
    @observable
    dayOfTheWeek: number
  }
  
}

export default Schedule
