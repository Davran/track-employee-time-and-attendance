import {observable} from 'mobx'
import {attr} from '../common/annotations/model'
import Schedule from './schedule'
import PunchReport from './punchReport'

namespace Employee {
  export class Model{
    @attr()
    @observable
    id: string
  
    @attr()
    @observable
    createdAt: number
  
    @attr()
    @observable
    updatedAt: number
    
    @attr()
    @observable
    employeeNumber: number
    
    @attr()
    @observable
    firstName: string
    
    @attr()
    @observable
    lastName: string
    
    @attr()
    @observable
    status: string
  
    @attr()
    @observable
    street: string
    
    @attr()
    @observable
    city: string
    
    @attr({optional: true})
    @observable
    nextPlannedPunch: string
  }
  
  export class ModelWithSchedule {
  
    @attr()
    @observable
    id: string
  
    @attr()
    @observable
    createdAt: number
  
    @attr()
    @observable
    updatedAt: number
  
    @attr()
    @observable
    employeeNumber: number
  
    @attr()
    @observable
    firstName: string
  
    @attr()
    @observable
    lastName: string
  
    @attr()
    @observable
    status: string
  
    @attr()
    @observable
    city: string
    
    @attr()
    @observable
    street: string
  
    @attr({optional: true})
    @observable
    nextPlannedPunch: string
    
    @attr()
    @observable
    schedule: Schedule.Model[]
  }
  
  export class AddModel{
    @attr()
    @observable
    employeeNumber: number
    
    @attr()
    @observable
    latitude: number
    
    @attr()
    @observable
    longitude: number
    
    @attr()
    @observable
    photo: string
  }
  
  export class PunchResultReasons {
    @attr({optional: true})
    @observable
    city: string | null
    
    @attr({optional: true})
    @observable
    time: number | null
  }
  export class PunchResultModel {
    @attr()
    @observable
    isAllow: boolean
    
    @attr()
    @observable
    reasons: PunchResultReasons
  }
  
  export class ModelWithScheduleList {
    @attr()
    @observable
    all: ModelWithSchedule[]
    
    @attr()
    @observable
    count: number
  }
  
  export interface Records {
    reportById: PunchReport.Model,
    punchResult: Employee.PunchResultModel
    createdEmployee: Employee.Model
    allEmployee: Employee.ModelWithScheduleList
    employee: Employee.ModelWithSchedule
  }
}

export default Employee
