import ValidationForm from './mobx-validator'

const UserLogInValidator = (onSuccess?: Function) => ValidationForm(
  {
    fields: [
      {
        name: 'email',
        label: 'Email',
        placeholder: '',
        rules: 'required|email'
      },
      {
        name: 'password',
        label: 'Password',
        placeholder: '',
        rules: 'required'
      }
    ],
    hooks: {
      onSuccess: onSuccess
    }
  }
)

export default UserLogInValidator
