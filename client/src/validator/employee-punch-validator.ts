import ValidationForm from './mobx-validator'

const EmployeePunchValidator = (onSuccess?: Function) => ValidationForm(
  {
    fields: [
      {
        name: 'photo',
        label: '',
        rules: 'required|string'
      },
      {
        name: 'latitude',
        label: '',
        rules: 'required|numeric'
      },
      {
        name: 'longitude',
        label: '',
        rules: 'required|numeric'
      },
      {
        name: 'employeeNumber',
        label: 'Employee Number',
        rules: 'required|numeric'
      }
    ],
    hooks: {
      onSuccess: onSuccess
    }
  }
)

export default EmployeePunchValidator
