import ValidationForm from './mobx-validator'

const EmployeeValidator = (onSuccess?: Function) => ValidationForm(
  {
    fields: [
      {
        name: 'firstName',
        label: 'First Name',
        placeholder: '',
        rules: 'required'
      },
      {
        name: 'lastName',
        label: 'Last Name',
        placeholder: '',
        rules: 'required'
      },
      {
        name: 'city',
        label: 'City',
        placeholder: '',
        rules: 'required'
      },
      {
        name: 'street',
        label: 'Street',
        placeholder: '',
        rules: 'required'
      },
      {
        name: 'photo',
        label: 'photo',
        placeholder: '',
        rules: 'required'
      }
    ],
    hooks: {
      onSuccess: onSuccess
    }
  }
)

export default EmployeeValidator
