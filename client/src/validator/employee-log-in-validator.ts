import ValidationForm from './mobx-validator'

const EmployeeLogInValidator = (onSuccess?: Function) => ValidationForm(
  {
    fields: [
      {
        name: 'number',
        label: 'Employee Number',
        placeholder: '',
        rules: 'required|numeric'
      }
    ],
    hooks: {
      onSuccess: onSuccess
    }
  }
)

export default EmployeeLogInValidator
