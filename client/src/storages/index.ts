import Todo from '../models/todo'
import {RecordStorage} from '../common/storages/base'
import Post from '../models/post'
import Employee from '../models/employee'
import PunchReport from '../models/punchReport'

export interface TodoRecordStorage extends RecordStorage<Todo.Records> {
  newModel(): Todo.Model
  
  toggleTodo(todo: Todo.Model): this
  
  addNewToAllList(todo: Todo.Model): this
  
  getAll(): Todo.ModelList
}

export interface PostRecordStorage extends RecordStorage<Post.Records> {

}
export interface EmployeeRecordStorage extends RecordStorage<Employee.Records> {
  getReportById(): PunchReport.Model
  getPunchResult(): Employee.PunchResultModel
  getCreatedEmployee(): Employee.Model
  getAllEmployee(): Employee.ModelWithScheduleList
  getEmployee(): Employee.ModelWithSchedule
}
