import {EmployeeRecordStorage} from '../index'
import DefaultRecordStorage from '../../common/storages/base/default'
import {storage} from '../../common/annotations/common'
import Employee from '../../models/employee'
import {action} from 'mobx'
import PunchReport from '../../models/punchReport'

@storage
export default class DefaultEmployeeRecordStorage extends DefaultRecordStorage<Employee.Records> implements EmployeeRecordStorage {
  
  @action
  getReportById(): PunchReport.Model {
    return this.getWithDefault('reportById', {}, PunchReport.Model)._
  }
  
  @action
  getPunchResult(): Employee.PunchResultModel {
    return this.getWithDefault('punchResult', {},Employee.PunchResultModel)._
  }
  
  @action
  getCreatedEmployee(): Employee.Model {
    return this.getWithDefault('createdEmployee', {},Employee.Model)._
  }
  
  @action
  getAllEmployee(): Employee.ModelWithScheduleList {
    return this.getWithDefault('allEmployee', {}, Employee.ModelWithScheduleList)._
  }
  
  @action
  getEmployee(): Employee.ModelWithSchedule {
    return this.getWithDefault('employee', {}, Employee.ModelWithSchedule)._
  }
}
