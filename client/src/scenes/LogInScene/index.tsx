import * as React from 'react'
import {action} from 'mobx'
import DefaultInput from '../../components/functional/inputs/default-input'
import UserLogInValidator from '../../validator/user-log-in-validator'
import {AuthService} from '../../services'
import {instanceRegistry} from '../../common/annotations/common'
import {Redirect} from 'react-router'
import {observer} from 'mobx-react'

interface Props {
  userLogInForm: any
  history?: History
  location?: any
  match?: any
}

class LogInScene extends React.Component<Props, {}> {
  render() {
    return (
      <div className='employee_login'>
        <div className="box content_center employee_login_content">
          <h2 className="text_center">Employee Log In</h2>
          <span className="spacer"/>
          <form className="employee_login_form" onSubmit={this.props.userLogInForm.onSubmit}>
            <DefaultInput form={this.props.userLogInForm} field='email' type='email'/>
            <DefaultInput form={this.props.userLogInForm} field='password' type='password'/>
            <span className="spacer"/>
            <input type="submit" value="Log In" className='btn btn-primary'/>
          </form>
        </div>
      </div>
    )
  }
}

interface ContainerProps {
}


@observer
export default class LogInSceneContainer extends React.Component<ContainerProps, {}> {
  private authService: AuthService = instanceRegistry.get('AuthService')
  
  @action
  public login = (form: any) => {
    this.authService.login(form.$('email').value, form.$('password').value)
  }
  
  private userLogInForm = UserLogInValidator(this.login)
  
  render() {
    if(this.authService.isLogged()){
      return (<Redirect to='/boss' />)
    }
    return (
      <div>
        <LogInScene userLogInForm={this.userLogInForm}/>
      </div>
    )
  }
}
