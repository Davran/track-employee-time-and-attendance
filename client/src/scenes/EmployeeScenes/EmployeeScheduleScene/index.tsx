import * as React from 'react'
import './index.scss'
import Table from '../../../components/functional/table'

interface Props {
  history?: History
  location?: any
  match?: any
}

class EmployeeScheduleScene extends React.Component<Props, {}> {
  render() {
    return (
      <div className="box content_center employee_schedule_content">
        <h2 className="text_center">Employee Schedule</h2>
        <span className="spacer"/>
        <p>Employee Number:</p>
        <p className="employee_id">123456789</p>
        <br/>
        <p>Employee Status:</p>
        <p className="punched_in">Punched In</p>
        <span className="spacer"/>
        <Table columns={tableColumn} data={tableData} sortable={false} className='employee-schedule-table'/>
      </div>
    )
  }
}

interface ContainerProps {
}

export default class EmployeeScheduleSceneContainer extends React.Component<ContainerProps, {}> {
  render() {
    return (
      <EmployeeScheduleScene/>
    )
  }
}

const tableColumn = [
  {
    Header: "Day Of The Week",
    accessor: "dayOfTheWeek"
  },
  {
    Header: "Start at",
    accessor: "startAt",
    className: 'text-center'
  },
  {
    Header: "End At",
    accessor: "endAt",
    className: 'text-center'
  }
]

const tableData = [
  {
    dayOfTheWeek: 'Monday',
    startAt: '00:00',
    endAt: '00:00'
  },
  {
    dayOfTheWeek: 'Thursday',
    startAt: '00:00',
    endAt: '00:00'
  },
  {
    dayOfTheWeek: 'Thursday',
    startAt: '00:00',
    endAt: '00:00'
  },
  {
    dayOfTheWeek: 'Wednesday',
    startAt: '00:00',
    endAt: '00:00'
  },
  {
    dayOfTheWeek: 'Friday',
    startAt: '00:00',
    endAt: '00:00'
  },
  {
    dayOfTheWeek: 'Saturday',
    startAt: '00:00',
    endAt: '00:00'
  },
  {
    dayOfTheWeek: 'Sunday',
    startAt: '00:00',
    endAt: '00:00'
  },
]
