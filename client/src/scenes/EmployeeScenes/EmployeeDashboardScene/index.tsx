import * as React from 'react'
import Webcam = require('react-webcam')
import EmployeePunchValidator from '../../../validator/employee-punch-validator'
import DefaultInput from '../../../components/functional/inputs/default-input'
import {instanceRegistry} from '../../../common/annotations/common'
import {EmployeeService} from '../../../services'
import {action, observable} from 'mobx'
import {observer} from 'mobx-react'

interface Props {
  form: any
  onSubmit?: any
  history?: History
  location?: any
  match?: any
}

interface State {
  imageError: boolean
  userPhoto?: string
  latitude?: number
  longitude?: number
  geolocationError?: boolean
}

class EmployeeDashboardScene extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.setToForm('employeeNumber', 356350052)
    this.state = {
      imageError: false,
      geolocationError: false
    }
  }
  
  componentDidMount() {
    this.setLocation()
  }
  
  webcam: any
  
  setRef = (webcam: any) => {
    this.webcam = webcam
  }
  onSubmit = async (e: any) => {
    e.preventDefault()
    const image = this.webcam.getScreenshot()
    if (!image) {
      await this.setState({imageError: true})
      return
    }
    this.setToForm('photo', image)
    
    this.props.form.onSubmit(e)
  }
  
  setLocation = async () => {
    if (navigator.geolocation) {
      return await navigator.geolocation.getCurrentPosition((position) => {
        //TODO Need to pass as value to default input
        this.setToForm('latitude', position.coords.latitude)
        this.setToForm('longitude', position.coords.longitude)
        return
      })
    } else {
      return await this.setState({geolocationError: true})
    }
  }
  
  setToForm = (field: string, value: any) => {
    this.props.form.$(field)
        .set(value)
  }
  
  render() {
    const imageError = (this.state.imageError) ?
      <span className='error'>Can not take photo. Please check camera</span> : null
    const geolocationError = (this.state.geolocationError) ?
      <span className='error'>Geolocation is not supported by this browser</span> : null
    
    return (
      <div className="box content_center employee_dash_content">
        <h2 className="text_center">Employee Dashboard</h2>
        <span className="spacer"/>
        <p>Employee Number:</p>
        <p className="employee_id">{this.props.form.$('employeeNumber').value}</p>
        <br/>
        <p>Employee Status:</p>
        <p className="punched_out">Punched Out</p>
        <span className="spacer"/>
        <form className="employee_dash_form" onSubmit={this.onSubmit}>
          <Webcam
            audio={false}
            height={350}
            ref={this.setRef}
            screenshotFormat="image/jpeg"
            width={350}
          />
          
          <DefaultInput form={this.props.form} field='employeeNumber'/>
          <DefaultInput form={this.props.form} field='latitude' type='hidden'/>
          <DefaultInput form={this.props.form} field='longitude' type='hidden'/>
          <DefaultInput form={this.props.form} field='photo' type='hidden'/>
          <DefaultInput form={this.props.form} field='photo' type='hidden'/>
          <p className="desktop">Punch In Functionality Requires A Mobile Or Tablet</p>
          <span className="spacer"/>
          {imageError}
          {geolocationError}
          <input className="btn btn-success" type="submit" value="Punch In/Out"/>
        </form>
      </div>
    )
  }
}

interface ContainerProps {
}

@observer
export default class EmployeeDashboardSceneContainer extends React.Component<ContainerProps, {}> {
  
  @action
  public handleSubmit = ((form: any) => {
    this.employeeService.punch(form)
  })
  
  private employeeService: EmployeeService = instanceRegistry.get('EmployeeService')
  private employeePunchForm = EmployeePunchValidator(this.handleSubmit)
  
  render() {
    return (
      <EmployeeDashboardScene form={this.employeePunchForm}/>
    )
  }
}
