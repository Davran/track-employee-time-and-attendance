import * as React from 'react'
import {NotFoundScene} from '../NotFoundScene'
import {Route, Switch} from 'react-router'
import EmployeeScheduleSceneContainer from './EmployeeScheduleScene'
import EmployeeDashboardSceneContainer from './EmployeeDashboardScene'

interface Props {
  history?: History
  location?: any
  match?: any
}

export default class EmployeeScene extends React.Component<Props, {}> {
  render() {
    const {match} = this.props
    return (
      <div>
        <Switch>
          <Route path={`${match.path}/schedule`} component={EmployeeScheduleSceneContainer} exact/>
          <Route path={`${match.path}/dashboard`} component={EmployeeDashboardSceneContainer} exact/>
          <Route path="*" component={NotFoundScene}/>
        </Switch>
      </div>
    )
  }
}
