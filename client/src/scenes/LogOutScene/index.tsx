import * as React from 'react';
import {AuthService} from '../../services'
import {instanceRegistry} from '../../common/annotations/common'
import {Redirect} from 'react-router'

export class LogOutScene extends React.Component<{}, {}> {
  private authService: AuthService = instanceRegistry.get('AuthService')
  
  componentWillMount() {
    this.authService.logout()
  }
  
  render() {
    return (
      <Redirect to='/login'/>
    );
  }
}
