import * as React from 'react'
import {EmployeeService} from '../../../services'
import {instanceRegistry} from '../../../common/annotations/common'
import {EmployeeMapper} from '../../../mappers'
import Employee from '../../../models/employee'
import {observer} from 'mobx-react'
import Table from '../../../components/functional/table'
import LinkTo from '../../../components/ui/common/link-to'

interface Props {
  allEmployee?: Employee.ModelWithScheduleList
}

class AllEmployeesScene extends React.Component<Props, {}> {
  render() {
    const {props} = this
    if (!props.allEmployee) {
      return <div>
      
      </div>
    }
    const tableData = props.allEmployee.all.map(it => {
      return {
        employeeNumber: it.employeeNumber,
        city: it.city,
        fullName: `${it.firstName} ${it.lastName}`,
        status: it.status,
        edit: <LinkTo to={`/boss/employee/${it.id}`}>See schedule</LinkTo>
      }
    });
    return (
      <div className='box content_center employee_dash_content'>
        <h2 className="text_center">All employee</h2>
        <span className="spacer"/>
        <div>
          <Table data={tableData} columns={tableColumns}/>
        </div>
      </div>
    )
  }
}

interface ContainerProps {
  history?: History
  location?: any
  match?: any
}

@observer
export default class AllEmployeesSceneContainer extends React.Component<ContainerProps, {}> {
  private employeeService: EmployeeService = instanceRegistry.get('EmployeeService')
  private employeeMapper: EmployeeMapper = instanceRegistry.get('EmployeeMapper')
  
  componentWillMount() {
    this.employeeService.getAllEmployee()
  }
  
  render() {
    const allEmployee = this.employeeMapper.allEmployee
    return (
      <AllEmployeesScene allEmployee={allEmployee}/>
    )
  }
}

const tableColumns = [
  {
    Header: 'Employee #',
    accessor: 'employeeNumber',
    minWidth: 100,
  },
  {
    Header: 'Full Name',
    accessor: 'fullName',
    minWidth: 200,
  },
  {
    Header: 'City',
    accessor: 'city',
    minWidth: 100,
  },
  {
    Header: 'Status',
    accessor: 'status',
    minWidth: 100,
  },
  {
    Header: '',
    accessor: 'edit',
    minWidth: 100,
  },
]
