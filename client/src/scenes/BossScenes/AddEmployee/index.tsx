import * as React from 'react'
import {EmployeeService} from '../../../services'
import {instanceRegistry} from '../../../common/annotations/common'
import {EmployeeMapper} from '../../../mappers'
import {observer} from 'mobx-react'
import EmployeeValidator from '../../../validator/employee-validator'
import {action} from 'mobx'
import DefaultInput from '../../../components/functional/inputs/default-input'
import {Link} from 'react-router-dom'

interface Props {
  form: any
}

class AddEmployeeScene extends React.Component<Props, {}> {
  render() {
    return (
      <div className='box content_center employee_dash_content'>
        <h2 className="text_center">Add an employee</h2>
        <form className="employee_dash_form" onSubmit={this.props.form.onSubmit} encType='multipart/form-data'>
          <DefaultInput form={this.props.form} field='firstName'/>
          <DefaultInput form={this.props.form} field='lastName'/>
          <DefaultInput form={this.props.form} field='city'/>
          <DefaultInput form={this.props.form} field='street'/>
          <DefaultInput form={this.props.form} field='photo' type='file'/>
          <div className='btn-group'>
            <Link to={''} className='btn btn-danger'>Cancel</Link>
            <input type="submit" value='Create' className='btn btn-success'/>
          </div>
        </form>
      </div>
    )
  }
}

interface ContainerProps {
  history?: History
  location?: any
  match?: any
}

@observer
export default class AddEmployeeSceneContainer extends React.Component<ContainerProps, {}> {
  private employeeService: EmployeeService = instanceRegistry.get('EmployeeService')
  private employeeMapper: EmployeeMapper = instanceRegistry.get('EmployeeMapper')
  
  @action
  public handleSubmit = ((form: any) => {
    this.employeeService.create(form)
  })
  
  private form = EmployeeValidator(this.handleSubmit)
  
  render() {
    return (
      <AddEmployeeScene form={this.form}/>
    )
  }
}
