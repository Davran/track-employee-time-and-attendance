import * as React from 'react'
import {observer} from 'mobx-react'
import {EmployeeMapper} from '../../../mappers'
import {instanceRegistry} from '../../../common/annotations/common'
import {EmployeeService} from '../../../services'
import Employee from '../../../models/employee'
import Table from '../../../components/functional/table'

interface Props {
  employee?: Employee.ModelWithSchedule
}

class EmployeeScene extends React.Component<Props, {}> {
  render() {
    const {props} = this
    if (!props.employee) {
      return <div>
      
      </div>
    }
    
    // const schedule = props.employee.schedule.reduce((accumulator: any, currentValue: any) => {
    //   accumulator[currentValue.dayOfTheWeek] = currentValue
    //   return accumulator
    // }, [])
    const schedule = props.employee.schedule.map(it => {
      return Object.assign(it, {dayOfTheWeek: dayOfTheWeekNames[it.dayOfTheWeek]})
    })
  
    
    const tableData = props.employee.schedule.map(it => it)
  
    return (
      <div className='box content_center employee_dash_content'>
        <h2 className="text_center">Employee: {props.employee.firstName} {props.employee.lastName}</h2>
        <h2 className="text_center">Employee number: {props.employee.employeeNumber}</h2>
        <h2 className="text_center">Employee Address: {props.employee.city}, {props.employee.street}</h2>
        <span className="spacer"/>
        <div>
          <Table data={schedule} columns={tableColumns}/>
        </div>
      </div>
    )
  }
}

interface ContainerProps {
  history?: History
  location?: any
  match?: any
}

@observer
export default class EmployeeSceneContainer extends React.Component<ContainerProps, {}> {
  private employeeService: EmployeeService = instanceRegistry.get('EmployeeService')
  private employeeMapper: EmployeeMapper = instanceRegistry.get('EmployeeMapper')
  
  componentWillMount() {
    this.employeeService.getEmployee(this.props.match.params.id)
  }
  
  render() {
    const employee = this.employeeMapper.employee
    
    return (
      <EmployeeScene employee={employee}/>
    )
  }
}

const tableColumns = [
  {
    Header: 'Day of the week',
    accessor: 'dayOfTheWeek',
    minWidth: 150
  },
  {
    Header: 'Start At',
    accessor: 'startAt'
  },
  {
    Header: 'End At',
    accessor: 'endAt'
  },
]


const dayOfTheWeekNames = ['Saturday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
