import * as React from 'react'
import {NotFoundScene} from '../NotFoundScene'
import {Redirect, Route, Switch} from 'react-router'
import AllEmployeesSceneContainer from './AllEmployees'
import EmployeeSceneContainer from './Employee'
import AddEmployeeSceneContainer from "./AddEmployee";
import {instanceRegistry} from '../../common/annotations/common'
import {AuthService} from '../../services'

interface Props {
  history?: History
  location?: any
  match?: any
}

export default class BossScene extends React.Component<Props, {}> {
  private authService: AuthService = instanceRegistry.get('AuthService')
  
  render() {
    const {match} = this.props
    if(!this.authService.isLogged()){
      return (<Redirect to='/login'/>)
    }
    return (
      <div>
        <Switch>
          <Route path={`${match.path}`} component={AllEmployeesSceneContainer} exact/>
          <Route path={`${match.path}/employee/new`} component={AddEmployeeSceneContainer} exact/>
          <Route path={`${match.path}/employee/:id`} component={EmployeeSceneContainer} exact/>
          <Route path="*" component={NotFoundScene}/>1
        </Switch>
      </div>
    )
  }
}
