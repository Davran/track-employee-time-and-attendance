import * as React from 'react'
import {instanceRegistry} from '../../common/annotations/common'
import {EmployeeService} from '../../services'
import {EmployeeMapper} from '../../mappers'
import {observer} from 'mobx-react'
import PunchReport from '../../models/punchReport'
import Moment from 'react-moment'

interface Props {
  report?: PunchReport.Model,
  allowPunchRequest: () => void
  disallowPunchRequest: () => void
}

class ReportScene extends React.Component<Props, {}> {
  render() {
    const {props} = this
    if (!props.report) {
      return <div>
      
      </div>
    }
    
    const wrongCity = (props.report.reasons.city) ? <p>An employee was in wrong city</p> : null
    const wrongTime = (props.report.reasons.time) ? <p>An employee
      punched {props.report.action} {getTimeFromMins(props.report.reasons.time)} {(props.report.action === 'in') ? 'late' : 'early'} </p> : null
    const actionButtons = (props.report.status === 'pending') ?
      <div className='btn-group'>
        <button className='btn btn-success' onClick={this.props.allowPunchRequest}>Allow</button>
        <button className='btn btn-danger' onClick={this.props.disallowPunchRequest}>Disallow</button>
      </div> : null
    return (
      <div className='box content_center employee_dash_content'>
        <h2 className="text_center">Employee Punch Report</h2>
        <span className="spacer"/>
        <div>
          <h4 className='text-left'>Employee Number: {props.report.employee.employeeNumber}</h4>
          <h4 className='text-left'>Employee Full
            Name: {props.report.employee.firstName} {props.report.employee.lastName}</h4>
          <h4 className='text-left'>City: {props.report.city}</h4>
          <h4 className='text-left'>Action: Punch {props.report.action}</h4>
          <h4 className='text-left text-capitalize'>Status: {props.report.status}</h4>
          <h4 className='text-left text-capitalize'>Reason:</h4>
          {wrongCity}
          {wrongTime}
          <h4 className='text-left'>Photo:</h4>
          <img src={props.report.photo} alt=""/>
          {actionButtons}
        </div>
      </div>
    )
  }
}

interface ContainerProps {
  history?: History
  location?: any
  match?: any
}

@observer
export default class ReportSceneContainer extends React.Component<ContainerProps, {}> {
  private employeeService: EmployeeService = instanceRegistry.get('EmployeeService')
  private employeeMapper: EmployeeMapper = instanceRegistry.get('EmployeeMapper')
  
  componentWillMount() {
    this.employeeService.getReportById(this.props.match.params.id)
  }
  
  allowPunchRequest = () => {
    this.employeeService.allowPunchRequest(this.props.match.params.id)
  }
  disallowPunchRequest = () => {
    this.employeeService.disallowPunchRequest(this.props.match.params.id)
  }
  
  render() {
    const report = this.employeeMapper.reportById
    return (
      <ReportScene report={report} allowPunchRequest={this.allowPunchRequest} disallowPunchRequest={this.disallowPunchRequest}/>
    )
  }
}

const getTimeFromMins = (min: number) => {
  if (min >= 24 * 60 || min < 0) {
    throw new RangeError('Valid input should be greater than or equal to 0 and less than 1440.')
  }
  const h = min / 60 | 0,
    m = min % 60 | 0
  
  return `${h} hour(s) and ${m} minute(s)`
}
