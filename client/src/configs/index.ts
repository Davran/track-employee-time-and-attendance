export const configuration = Object.freeze(
  {
    remoteApi: 'http://localhost:1337',
    tokenApi: 'http://localhost:1337/login',
    refreshTokenApi: 'http://localhost:8081/api/auth/token'
  }
)
