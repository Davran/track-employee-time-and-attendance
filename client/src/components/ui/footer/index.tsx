import * as React from 'react'

interface Props {
}

const Footer = (props: Props) => (
  <footer className="footer">
    <nav className="footer_navigation">
      <div className="navigation_menu">
        <a className="menu_link" href="">&copy; Time Tracker 2018</a>
      </div>
    </nav>
  
  </footer>
)

export default Footer
