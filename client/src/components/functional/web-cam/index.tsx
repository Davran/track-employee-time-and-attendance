import * as React from 'react'
import Webcam = require('react-webcam')

interface Props {
  capture: (picture: string) => void
  history?: History
  location?: any
  match?: any
}

interface State {
  error?: boolean
}

export default class WebCamBlock extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      error: false
    }
  }
  
  webcam: any
  
  setRef = (webcam: any) => {
    this.webcam = webcam
  }
  
  takePhoto = () => {
    this.props.capture(this.webcam.getScreenshot());
  }
  
  onUserMedia = () => {
  
  }
  
  render() {
    return (
      <div>
        <Webcam
          audio={false}
          height={350}
          ref={this.setRef}
          screenshotFormat="image/jpeg"
          width={350}
        />
      </div>
    )
  }
}
