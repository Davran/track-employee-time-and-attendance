import * as React from 'react'
import {slide as Menu} from 'react-burger-menu'
import MenuItem from './menu-item'
import './index.scss'

const TopMenu = () => {
  
  const mainMenu = menuItems.map((it, i) =>
                                   (<MenuItem key={i} to={`/${it.to}`} className='menu-item'>{it.name}</MenuItem>))
  
  return (
    <Menu right burgerButtonClassName='right'>
      <nav className='bt-item-list'>
        {mainMenu}
      </nav>
    </Menu>
  )
}

const menuItems = [
  {
    name: 'Employees',
    to: ''
  },
  {
    name: 'Reports',
    to: 'reports'
  },
  {
    name: 'settings',
    to: 'settings'
  },
  {
    name: 'Punch In',
    to: ''
  },
  {
    name: 'Logout',
    to: 'logout'
  }
]

export default TopMenu

