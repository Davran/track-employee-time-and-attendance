import * as React from 'react'
import './index.scss'
import * as classnames from 'classnames'
import {observer} from 'mobx-react'

interface DefaultInputProps {
  onChange?: (event: any) => void
  placeholder?: string
  form?: any
  field?: string
  value?: string | number | null
  errorMessage?: string
  type?: 'text' | 'number' | 'email' | 'file' | 'date' | 'checkbox' | 'hidden' | 'password'
  label?: string
  disabled?: boolean
  mask?: string
}

const onChangeHandler = (props: DefaultInputProps, bindings: any) => (event: any) => {
  // pass event to user defined callbacks (if any)
  props.onChange && props.onChange(event)
  // pass event to validation (if any)
  bindings && bindings.onChange(event)
}

const DefaultInput = observer((props: DefaultInputProps) => {
  const {form, field, placeholder} = props
  
  
  const formExist = form && field && form.$(field)
  const label = (formExist && props.type !== 'hidden') ? form.$(field).label : props.label
  const value = formExist ? form.$(field).value : props.value
  const errorMessage = (formExist && props.type !== 'hidden') ? form.$(field).error : props.errorMessage
  const bindings = formExist ? form.$(field)
                                   .bind() : {}
  const type = props.type || 'text'
  
  const classes = classnames({
                               'default-input': true,
                               'has-error': errorMessage,
                               'not-empty': value
                             })
  
  const wrapperClasses = classnames({
                                      'default-input-wrapper': true,
                                      'has-error': errorMessage
                                    })
  
  const labelElement = label ? <label htmlFor={`default-input-${props.field}`}>{label}:</label> : null
  
  // console.log(props.value, ' => ', form.$(field).value)
  
  
  return (
    <div className={wrapperClasses}>
      {labelElement}
      <input
        {...bindings}
        id={`default-input-${props.field}`}
        className={classes}
        placeholder={placeholder}
        value={value}
        onChange={onChangeHandler(props, bindings)}
        type={type}
        disabled={props.disabled}
      />
      <div className="error-message">{errorMessage}</div>
    </div>
  )
})

export default DefaultInput
