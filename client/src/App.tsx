import * as React from 'react'
import {Route, Router, Switch} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import './vendor/styles/application.scss'

import {NotFoundScene} from './scenes/NotFoundScene'
import LogInSceneContainer from './scenes/LogInScene'
import TopMenu from './components/functional/menu'
import Footer from './components/ui/footer'
import EmployeeScene from './scenes/EmployeeScenes'
import ReportSceneContainer from './scenes/ReportScene'
import BossScene from './scenes/BossScenes'
import {LogOutScene} from './scenes/LogOutScene'

const history = createBrowserHistory()

export default class App extends React.Component<{}, {}> {
  
  constructor(props: {}, context?: any) {
    super(props, context)
    console.info('react started render')
  }
  
  render() {
    return (
      <Router history={history}>
        <div className='page'>
          <main className='content'>
            <TopMenu/>
            <Switch>
              <Route path="/login" exact component={LogInSceneContainer}/>
              <Route path="/logout" exact component={LogOutScene}/>
              <Route path="/employee" component={EmployeeScene}/>
              <Route path="/boss" component={BossScene}/>
              <Route path="/report/:id" component={ReportSceneContainer}/>
              <Route path="/error" component={NotFoundScene}/>
              <Route path="*" component={NotFoundScene}/>
            </Switch>
          </main>
          <Footer/>
        </div>
      </Router>
    )
  }
}

