import {configuration} from '../configs'

const base = (rest: string) => `${configuration.remoteApi}/${rest}`

export default class Paths {
  static Post = class {
    static byId = (postId: number) => base(`posts/${postId}`)
    static all = base('posts')
  }
  static Employee = class {
    static create = () => base(`employee/create`)
    static getAllEmployee = () => base(`employee`)
    static getEmployee = (id: string) => base(`employee/${id}`)
    static punch = (employeeNumber: number) => base(`employee/punch/${employeeNumber}`)
    static getReport = (reportId: string) => base(`employee/report/${reportId}`)
    static allowPunchRequest = (reportId: string) => base(`employee/allow-punch-request/${reportId}`)
    static disallowPunchRequest = (reportId: string) => base(`employee/disallow-punch-request/${reportId}`)
  }
}
