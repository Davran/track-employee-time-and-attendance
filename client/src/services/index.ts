import Todo from '../models/todo'
import BaseService from '../common/services/base/base'
import PunchReport from '../models/punchReport'

export interface TodoService extends BaseService {
  createNew(): void
  
  toggleTodo(todo: Todo.Model): void
}

export interface PostService extends BaseService {
  loadPost(postId: number): void
}

export interface AuthService extends BaseService {
  login(email: string, password: string): void
  
  checkToken(): Promise<void>
  
  isLogged(): boolean
  
  getAuthInfo(): any
  
  logout(): void
}

export interface EmployeeService extends BaseService {
  punch(form: any): void
  getReportById(reportId: string): void
  allowPunchRequest(reportId: string): void
  disallowPunchRequest(reportId: string): void
  create(form: any): void
  getAllEmployee(): void
  getEmployee(id: string): void
}
