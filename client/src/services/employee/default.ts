import {EmployeeService} from '../index'
import {injectConst, injectMethod, service} from '../../common/annotations/common'
import Paths from '../../dicts/pathes'
import {Fetcher} from '../../fetchers'
import BaseService from '../../common/services/base/base'
import {EmployeeRecordStorage} from '../../storages'
import Employee from '../../models/employee'
import PunchReport from '../../models/punchReport'

@service
export default class DefaultEmployeeService extends BaseService implements EmployeeService {
  
  private fetcher: Fetcher
  
  constructor(@injectConst('EmployeeRecordStorage') private store: EmployeeRecordStorage) {
    super()
  }
  
  @injectMethod('Fetcher')
  setFetcher(fetch: Fetcher) {
    this.fetcher = fetch
  }
  
  async punch(form: any) {
    const response =  await this.fetcher.post(Paths.Employee.punch(form.$('employeeNumber').value), form.values(), Employee.PunchResultModel)
    this.store.set('punchResult', response)
  }
  
  async getReportById(reportId: string) {
    const response = await this.fetcher.get(Paths.Employee.getReport(reportId), undefined, PunchReport.Model);
    this.store.set('reportById', response)
  }
  
  async allowPunchRequest(reportId: string) {
    const response = await this.fetcher.post(Paths.Employee.allowPunchRequest(reportId), undefined, PunchReport.Model);
    this.store.set('reportById', response)
  }
  
  async disallowPunchRequest(reportId: string) {
    const response = await this.fetcher.post(Paths.Employee.allowPunchRequest(reportId), undefined, PunchReport.Model);
    this.store.set('reportById', response)
  }
  
  async create(form: any) {
    const response = await this.fetcher.post(Paths.Employee.create(), form.values(), Employee.Model);
    this.store.set('createdEmployee', response)
  }
  
  async getAllEmployee() {
    const response = await this.fetcher.get(Paths.Employee.getAllEmployee(), undefined);
    this.store.set('allEmployee', response)
  }
  
  async getEmployee(id: string) {
    const response = await this.fetcher.get(Paths.Employee.getEmployee(id), undefined);
    this.store.set('employee', response)
  }
}
