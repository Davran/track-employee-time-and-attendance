import Todo from '../models/todo'
import Post from '../models/post'
import {Maybe} from '../common/types'
import PunchReport from '../models/punchReport'
import Employee from '../models/employee'

export interface TodoMapper {
  
  all: Todo.Model[]
  
  unfinishedTodoCount: number
  
  lastOne: Maybe<Todo.Model>
}

export interface PostMapper {
  
  postById: Post.Model | undefined
}

export interface EmployeeMapper {
  reportById: PunchReport.Model | undefined
  punchResult: Employee.PunchResultModel | undefined
  createdEmployee: Employee.Model | undefined
  allEmployee: Employee.ModelWithScheduleList | undefined
  employee: Employee.ModelWithSchedule | undefined
}
