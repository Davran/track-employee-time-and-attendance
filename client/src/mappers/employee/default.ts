import {EmployeeMapper} from '../index'
import {injectConst, mapper} from '../../common/annotations/common'
import {computed} from 'mobx'
import {EmployeeRecordStorage} from '../../storages'

@mapper
export default class DefaultEmployeeMapper implements EmployeeMapper {
  
  constructor(@injectConst('EmployeeRecordStorage') protected store: EmployeeRecordStorage) {
  
  }
  
  @computed
  get reportById() {
    return this.store.get('reportById')._
  }
  
  @computed
  get punchResult() {
    return this.store.get('punchResult')._
  }
  
  @computed
  get createdEmployee() {
    return this.store.get('createdEmployee')._
  }
  
  @computed
  get allEmployee() {
    return this.store.get('allEmployee')._
  }
  
  @computed
  get employee() {
    return this.store.get('employee')._
  }
}
