const ScheduleService = require("./ScheduleService");
const PunchReportService = require("./PunchReportService");
const Twilio = require('twilio');
const NodeGeocoder = require('node-geocoder');
const moment = require('moment');

const client = new Twilio(sails.config.TWILIO_ACCOUNT_SID, sails.config.TWILIO_API_TOKEN);


class EmployeeService {
  static async sendNotification({employee, reportId}) {
    return await client.messages.create({
      body: this.punchActionMessage({employee, reportId}),
      to: '+996702188575',  // Text this number
      from: sails.config.TWILIO_PHONE_NUMBER // From a valid Twilio number
    });
  }

  static punchActionMessage({employee, reportId}) {
    const link = `${sails.config.FRONT_END_PATH}/report/${reportId}`;
    const schedule = `${employee.schedule[0].startAt} - ${employee.schedule[0].endAt}`;
    const status = this.getReversedStatus({status: employee.status});

    return `${employee.firstName} ${employee.lastName} wants to punch ${status}. \nSchedule: ${schedule}. \nPlease go to ${link} to take action`
  }

  static getReversedStatus({status}) {
    return (status === 'in') ? "out" : 'in';
  };


  static async getLocationByLatLon({lat, lon}) {
    const options = {
      provider: 'google',
      httpAdapter: 'https', // Default
      apiKey: sails.config.GOOGLE_MAP_KEY, // for Mapquest, OpenCage, Google Premier
      formatter: null         // 'gpx', 'string', ...
    };

    const geocoder = NodeGeocoder(options);

    return await geocoder.reverse({lat, lon})
      .catch(function (err) {
        console.log(err);
      });
  }

  static getIsAllowToPunch({employee, currentCity, timeDiff}) {
    const isInSuggestedCity = this.getNeededLocation({currentCity, neededCity: employee.city});

    const isOnTime = this.getIsOnTime({timeDiff, status: employee.status});

    return {
      isAllow: (isInSuggestedCity.status && isOnTime.status),
      reasons: {
        time: isOnTime.reason,
        city: isInSuggestedCity.reason
      }
    };
  };


  static getNeededLocation({currentCity, neededCity}) {
    const result = (neededCity.toLowerCase() === currentCity.toLowerCase());
    return {
      status: result,
      reason: (result) ? null : 'wrong city'
    }
  }

  static getIsOnTime({status, timeDiff}) {
    if (status === 'out') {
      return {
        status: (timeDiff <= 0),
        reason: (timeDiff <= 0) ? null : timeDiff
      };
    }
    return {
      status: (timeDiff >= 0),
      reason: (timeDiff >= 0) ? null : timeDiff
    };
  }

  /**
   *
   * @param employee
   * @returns {{isOnTime: boolean, reason: number | null}}
   */

  static async getTimeDiff({employee}) {
    const employeePlanedPunch = employee.nextPlannedPunch || await ScheduleService.getTodayPlannedPunchIn({employee});
    const currentTime = moment().format();
    const nextPlannedPunch = moment(employeePlanedPunch).format();
    return moment(currentTime).diff(nextPlannedPunch, 'minute');
  };

  static async tryPunch({employee, lat, lon, photo}) {
    const location = await EmployeeService.getLocationByLatLon({lat, lon}).catch(error => sails.log.error(error));
    console.log(location);

    const timeDiff = await this.getTimeDiff({employee});

    const isAllowedResult = this.getIsAllowToPunch({
      employee,
      currentCity: location[0].city,
      timeDiff: timeDiff
    });

    const report = await PunchReportService.createReport({
      lat, lon, photo,
      employee: employee.id,
      city: location[0].city,
      action: this.getReversedStatus({status: employee.status}),
      status: (isAllowedResult.isAllow) ? 'allowed' : 'pending',
      reasons: isAllowedResult.reasons,
      schedule: employee.schedule[0].id
    }).catch(error => sails.log.error(error));

    if (isAllowedResult.isAllow) {
      const nextPlannedPunch = await ScheduleService.getNextPlannedPunch({employee});
      await Employee.update({id: employee.id})
        .set({status: this.getReversedStatus({status: employee.status}), nextScheduledTime: nextPlannedPunch})
        .catch(error => sails.log.error(error));
    }
    await this.notifyAdminAboutPunch({employee, reportId: report.id}).catch(error => sails.log.error(error));
    return isAllowedResult
  }

  static async notifyAdminAboutPunch({employee, reportId}) {
    // EmployeeService.sendNotification({employee, reportId}).catch(error => sails.log.error(error))
  }

  static async getEmployeeWithTodayScheduleByEmployeeNumber({employeeNumber}) {
    return await Employee.findOne({employeeNumber})
      .populate('schedule', {
        where: {
          dayOfTheWeek: new Date().getDay()
        }
      }).catch(error => sails.log.error(error));
  }
}

module.exports = EmployeeService;
