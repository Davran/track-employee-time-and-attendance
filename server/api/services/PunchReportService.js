class PunchReportService {

  /**
   *
   * @returns {Promise<*>}
   * @param parameters
   */
  static async createReport(parameters) {
    return await PunchReport.create(parameters)
      .fetch()
      .catch(error => sails.log.error(error));
  }
}

module.exports = PunchReportService;
