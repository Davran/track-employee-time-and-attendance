const moment = require('moment');

class ScheduleService {
  static async setDefaultSchedule(employee) {
    await [0, 1, 2, 3, 4, 5, 6].forEach(async (it) => {
      await Schedule.create({
        employee: employee.id,
        dayOfTheWeek: it
      });
    });
    // const schedule = await Schedule.findOne({employee: employee.id, dayOfTheWeek: new Date().getDay()});
    // const nextPlannedPunch = this.getNextPlannedPunch({nextScheduledTime: schedule.startAt});
    // await Employee.update({id: employee.id}).set({nextPlannedPunch: nextPlannedPunch});
  };

  static async getNextPlannedPunch({employee}) {
    const nextWeekDay = (new Date().getDay() + 1 > 6) ? 0 : new Date().getDay() + 1;
    const neededDayOfTheWeek = (employee.status === 'in') ? new Date().getDay() : nextWeekDay;
    const schedule = await Schedule.findOne({employee: employee.id, dayOfTheWeek: neededDayOfTheWeek});
    return getFormattedNextScheduledTime({
      nextScheduledTime: (employee.status === 'out') ? schedule.endAt : schedule.startAt,
      isNextDay: (employee.status === 'in')
    })
  }

  static async getTodayPlannedPunchIn({employee}) {
    const schedule = await Schedule.findOne({employee: employee.id, dayOfTheWeek: new Date().getDay()});
    return getFormattedNextScheduledTime({nextScheduledTime: schedule.startAt})
  }
}


const getFormattedNextScheduledTime = ({nextScheduledTime, isNextDay}) => {
  const dateClass = new Date();
  let date = (isNextDay) ? moment(dateClass).add(1, 'days') : moment(dateClass);
  date = date.format('YYYY-MM-DD');
  return `${date} ${nextScheduledTime}:00`
};

module.exports = ScheduleService;
