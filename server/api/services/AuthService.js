const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const secret = 'd-"~X9ct~m:%v<eE8hd%5Xn<NWVhm}@mR:Cjj$+y';

class AuthService {
  static cryptPassword(password) {
    return bcrypt.hashSync(password, 10);
  }

  static checkPassword(password, hash){
    return bcrypt.compareSync(password, hash);
  }
  static generateToken(user) {
    const expiration = 7 * 24 * 60;

    return jwt.sign({
      exp: + new Date() + expiration,
      user: user
    }, secret);
  }
}

module.exports = AuthService;
