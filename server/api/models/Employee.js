/**
 * Employee.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const ScheduleService = require("../services/ScheduleService");

module.exports = {

  attributes: {
    employeeNumber: {
      type: 'number',
      unique: true,
    },

    firstName: {
      type: 'string',
    },

    lastName: {
      type: 'string',
    },

    status: {
      type: 'string',
      isIn: ['in', 'out'],
      defaultsTo: 'out'
    },

    city: {
      type: 'string',
    },

    street: {
      type: 'string',
    },

    photo: {
      type: 'string',
    },

    nextPlannedPunch: {
      type: 'string',
      columnType: 'date',
      allowNull: true,
    },

    schedule: {
      collection: 'schedule',
      via: 'employee'
    },

    punchReports: {
      collection: 'punchReport',
      via: 'employee'
    },
  },

  async beforeCreate(valuesToSet, proceed) {
    valuesToSet.employeeNumber = await getUniqueNumber();
    return proceed();
  },

  async afterCreate(values, proceed) {
    await ScheduleService.setDefaultSchedule(values);
    return proceed()
  }
};

const getUniqueNumber = async () => {
  const uniqueNumber = Math.floor(Math.random() * 100000);
  const employee = await Employee.findOne({employeeNumber: uniqueNumber});
  return (employee === undefined) ? uniqueNumber : getUniqueNumber();
};

