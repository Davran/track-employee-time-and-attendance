module.exports = {

  attributes: {
    photo: {
      type: 'string'
    },

    lat: {
      type: 'number'
    },

    lon: {
      type: 'number'
    },

    city: {
      type: 'string'
    },

    action: {
      type: 'string',
      isIn: ['in', 'out']
    },

    status: {
      type: 'string',
      isIn: ['allowed', 'disallowed', 'pending'],
      defaultsTo: 'pending'
    },

    reasons: {
      type: 'json',
      defaultsTo: {}
    },

    employee: {
      model: 'employee',
    },

    schedule: {
      model: 'schedule',
    }
  }
};
