/**
 * Schedule.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    dayOfTheWeek: {
      type: 'number',
      isIn: [0, 1, 2, 3, 4, 5, 6]
    },
    startAt: {
      type: 'string',
      defaultsTo: '09:00'
    },
    endAt: {
      type: 'string',
      defaultsTo: '18:00'
    },
    punchReports: {
      collection: 'punchReport',
      via: 'schedule'
    },
    employee: {
      model: 'employee',
    }
  },

};

