/**
 * User.js
 *
 * A user who can log in to this application.
 */

const AuthService = require("../services/AuthService");

module.exports = {

  attributes: {

    email: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 200,
    },

    password: {
      type: 'string',
      required: true,
      protect: true,
    },

    firstName: {
      type: 'string',
      required: true,
      maxLength: 120,
    },

    lastName: {
      type: 'string',
      required: true,
      maxLength: 120,
    },

    isSuperAdmin: {
      type: 'boolean',
      defaultsTo: false
    },

    passwordResetToken: {
      type: 'string',
    },

    passwordResetTokenExpiresAt: {
      type: 'number',
      allowNull: true
    },
  },

  beforeCreate(value, cb) {
    value.password = AuthService.cryptPassword(value.password);
    return cb();
  }


};
