const AuthService = require("../services/AuthService");

const login = async (req, res) => {

  const user = await User.findOne({email: req.body.email});

  if (!user) {
    return res.notFound()
  }

  if (!AuthService.checkPassword(req.body.password, user.password)){
    return res.notFound()
  }

  delete user.password;

  const token = AuthService.generateToken(user);

  res.ok({token: `Bearer ${token}`})
};

module.exports = {
  login
};
