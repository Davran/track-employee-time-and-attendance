const AuthService = require("../services/AuthService");

const create = async (req, res) => {
  try {
    const user = await User.create(req.body).fetch();

    delete user.password;

    const token = AuthService.generateToken(user);

    res.ok({user, token})
  } catch (error) {
    return res.badRequest(error)
  }
};

module.exports = {
  create
};
