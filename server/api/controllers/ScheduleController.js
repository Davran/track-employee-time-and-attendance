/**
 * ScheduleController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const getScheduleByEmployee = (req, res) => {
  res.json(200, {
    awd: 'awd'
  })
};

module.exports = {
  getScheduleByEmployee
};

