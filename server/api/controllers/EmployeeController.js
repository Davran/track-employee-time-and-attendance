/**
 * EmployeeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const ScheduleService = require("../services/ScheduleService");
const EmployeeService = require("../services/EmployeeService");

const getAll = async (req, res) => {
  const allEmployee = await Employee.find().populate('schedule');
  return res.status(200).json({
    all: allEmployee,
    count: allEmployee.length
  })
};

const get = async (req, res) => {
  const employee = await Employee.findOne({id: req.params.id}).populate('schedule');
  return res.status(200).json(employee)
};

const create = async (req, res) => {
  // const newEmployee = await Employee.create(req.body).meta({fetch: true}).fetch();

  req.file('photo').upload({
    // don't allow the total upload size to exceed ~10MB
    maxBytes: 10000000
  },function whenDone(err, uploadedFiles) {
    if (err) {
      return res.serverError(err);
    }

    // If no files were uploaded, respond with an error.
    if (uploadedFiles.length === 0){
      return res.badRequest('No file was uploaded');
    }

    // Get the base URL for our deployed application from our custom config
    // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
    var baseUrl = sails.config.custom.baseUrl;

    // Save the "fd" and the url where the avatar for a user can be accessed
    // User.update(req.session.userId, {
    //
    //   // Generate a unique URL where the avatar can be downloaded.
    //   avatarUrl: require('util').format('%s/user/avatar/%s', baseUrl, req.session.userId),
    //
    //   // Grab the first file and use it's `fd` (file descriptor)
    //   avatarFd: uploadedFiles[0].fd
    // })
    //   .exec(function (err){
    //     if (err) return res.serverError(err);
    //     return res.ok();
    //   });

  });

  // Employee.findOne().populate('schedule').exec((err, employee) => {
  //   return res.status(200).json({
  //     employee: employee,
  //   });
  // });
};

const punch = async (req, res) => {
  const employee = await EmployeeService.getEmployeeWithTodayScheduleByEmployeeNumber({employeeNumber: req.params.employeeNumber});
  if (!employee) {
    return res.notFound();
  }

  const result = await EmployeeService.tryPunch({
    employee,
    lat: req.body.latitude,
    lon: req.body.longitude,
    photo: req.body.photo
  });
  return res.status(200).json(result);
};

const getReportById = async (req, res) => {
  const employee = await PunchReport.findOne({id: req.params.punchReportId}).populate('employee').populate('schedule');
  if (!employee) {
    return res.notFound()
  }
  return res.status(200).json(employee)
};

const allowPunchRequest = async (req, res) => {
  const report = await PunchReport.update({id: req.params.punchReportId}).set({status: 'allowed'}).fetch();
  if (!report) {
    return res.notFound()
  }
  const punchReportWithEmployee = await PunchReport.findOne({id: req.params.punchReportId}).populate('employee');

  await Employee.update({id: punchReportWithEmployee.employee.id}).set({
    status: EmployeeService.getReversedStatus({status: punchReportWithEmployee.employee.status}),
    nextPlannedPunch: await ScheduleService.getNextPlannedPunch({employee: punchReportWithEmployee.employee})
  });
  const employee = await PunchReport.findOne({id: req.params.punchReportId}).populate('employee').populate('schedule');
  return res.status(200).json(employee)
};

const disallowPunchRequest = async (req, res) => {
  const report = await PunchReport.update({id: req.params.punchReportId}).set({status: 'disallowed'}).fetch();
  if (!report) {
    return res.notFound()
  }
  const employee = await PunchReport.findOne({id: req.params.punchReportId}).populate('employee').populate('schedule');
  return res.status(200).json(employee)
};

module.exports = {
  getAll,
  get,
  create,
  punch,
  getReportById,
  allowPunchRequest,
  disallowPunchRequest
};
